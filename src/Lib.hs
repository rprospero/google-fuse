module Lib
    ( someFunc
    ) where

import Control.Monad (foldM)
import Control.Monad.IO.Class (MonadIO)
import Data.ByteString (ByteString)
import qualified Data.ByteString.Char8 as B (pack, take, isPrefixOf, unpack)
import Data.Maybe (fromMaybe)
import Data.Time.Clock.POSIX (utcTimeToPOSIXSeconds)
import qualified Data.Text as T
import Network.Google.Api as G
import Network.Google.Drive.File as G
import Network.Google.Drive.Search as G
import Network.Google.OAuth2
import System.FilePath (splitFileName, splitDirectories)
import System.Fuse
import System.Posix.Files
import System.Posix.Types
import System.Process (readProcess)

logfile :: String
logfile = "/home/adam/Code/google-fuse/google.log"

defaultFile :: FileStat
defaultFile = FileStat {
  statEntryType = RegularFile
  , statFileMode = foldr1 unionFileModes
                   [ ownerReadMode
                   , ownerWriteMode
                   , groupReadMode
                   , otherReadMode
                   ]
  , statLinkCount = 1
  , statFileOwner = 0 -- fuseCtxUserID ctx
  , statFileGroup = 0 -- fuseCtxGroupID ctx
  , statSpecialDeviceID = 0
  , statFileSize = 0
  , statBlocks = 0
  , statAccessTime = 0
  , statModificationTime = 0
  , statStatusChangeTime = 0
  }

defaultDirectory :: FileStat
defaultDirectory = defaultFile {
  statEntryType = Directory
  , statFileMode = foldr1 unionFileModes
                   [ ownerReadMode
                   , ownerExecuteMode
                   , groupReadMode
                   , groupExecuteMode
                   , otherReadMode
                   , otherExecuteMode
                   ]
  , statLinkCount = 2
  , statFileSize = 4096
  }

googleFileStat :: FuseContext -> File -> FileStat
googleFileStat ctx f =
  (if isFolder f
   then defaultDirectory
   else defaultFile) {statModificationTime = mTime
                     ,statFileOwner = fuseCtxUserID ctx
                     ,statFileGroup = fuseCtxGroupID ctx
                     ,statFileSize = fromIntegral . fromMaybe 0 . G.fileSize . fileData $ f
                     }
  where
    mTime :: EpochTime
    mTime = fromIntegral rTime
    rTime :: Integer
    rTime = round . fromMaybe 0 $ utcTimeToPOSIXSeconds <$> (fileModified . fileData $ f)

client :: OAuth2Client
client = OAuth2Client "1062243630344-a99053pdrcahtdukp394op1545hrrec9.apps.googleusercontent.com" "PJz853FVmXeWycHpEaiNwjan"

scope :: [String]
scope = ["https://www.googleapis.com/auth/drive"]

googleToken :: IO OAuth2Token
googleToken = getAccessToken client scope (Just "/home/adam/.google-fuse.token")

googleFuse :: FuseOperations File
googleFuse = defaultFuseOps { fuseGetFileStat = googleGetFileStat
                            , fuseReadSymbolicLink = \_ -> logMessage "Read Symbolic Link" >> (return $ Left eNOSYS)
                            , fuseCreateDevice = googleCreateDevice
                            , fuseCreateDirectory = googleCreateFolder
                            , fuseRemoveLink = \_ -> logMessage "Remove Link" >> return eNOSYS
                            , fuseRemoveDirectory = \_ -> logMessage "Remove Directory" >> return eNOSYS
                            , fuseCreateSymbolicLink = \_ _ -> logMessage "Create Symbolic Link" >> return eNOSYS
                            , fuseRename = \_ _ -> logMessage "Rename" >> return eNOSYS
                            , fuseCreateLink = \_ _ -> logMessage "Read Create Link" >> return eNOSYS
                            , fuseSetFileMode = \_ _ -> logMessage "Set File Mode" >> return eNOSYS
                            , fuseSetOwnerAndGroup = \_ _ _ -> logMessage "Read Create Link" >> return eNOSYS
                            , fuseSetFileSize = \_ _ -> logMessage "Set File Size" >> return eOK
                            , fuseSetFileTimes = \_ _ _ -> logMessage "Set File Times" >> return eNOSYS

                            , fuseOpen        = googleOpen
                            , fuseRead        = googleRead
                            , fuseWrite       = googleWrite
                            , fuseOpenDirectory = googleOpenDirectory
                            , fuseReadDirectory = googleReadDirectory
                            , fuseGetFileSystemStats = googleGetFileSystemStats
                            , fuseFlush = \_ _ -> logMessage "Flush" >> return eOK
                            , fuseRelease = \_ _ -> logMessage "Release" >> return ()
                            , fuseSynchronizeFile = \_ _ -> logMessage "Synchonize File" >> return eNOSYS
                            , fuseReleaseDirectory = \_ -> logMessage "Release Directory" >> return eNOSYS
                            , fuseSynchronizeDirectory = \_ _ -> logMessage "Synchronoize Directory" >> return eNOSYS
                            , fuseAccess = \_ _ -> logMessage "Access" >> return eNOSYS
                            , fuseInit = logMessage "Init" >> return ()
                            , fuseDestroy = logMessage "Destroy" >> return ()
                            }

googleListDirectory :: File -> Api [File]
googleListDirectory path = do
  readDirectory . fileId $ path

googleFile :: FilePath -> Api (Maybe File)
googleFile = pathToFile

handleApi :: (MonadIO m, Monad m) => Api a -> b -> (a -> m b) -> m b
handleApi api myFail aftermath = do
  token <- liftIO googleToken
  result <- liftIO $ runApi token api
  case result of
    Left err -> do
      liftIO $ logMessage $ show err
      return myFail
    Right ans -> aftermath ans

handleMaybe :: Monad m => y -> (x -> m y) -> Maybe x -> m y
handleMaybe err action mx = do
  case mx of
    Nothing -> return $ err
    Just x -> action x

pathToFile :: FilePath -> Api (Maybe File)
pathToFile path = do
  Just root <- getFile $ T.pack "root"
  foldM action (Just root) $ steps path
    where
      action Nothing _ = return Nothing
      action (Just parent) name = do
        liftIO $ logMessage $ "Finding: " ++ T.unpack name ++ " from " ++ show (steps path) ++ " at " ++ show parent ++ " | " ++ path
        result <- listFiles (qIn (fileId parent) Parents ?&& Title ?= name)
        case result of
          [] -> return $ Nothing
          a:[] -> return $ Just a
          _:_ -> return $ Nothing -- Files should be unique
      steps = map T.pack . tail . splitDirectories

googleGetFileStat :: FilePath -> IO (Either Errno FileStat)
googleGetFileStat "/" = return . Right $ defaultDirectory
googleGetFileStat path = do
  ctx <- getFuseContext
  handleApi (googleFile path) (Left eNOENT) $ \stat -> do
    handleMaybe (Left eNOENT) (return . Right . googleFileStat ctx) stat

googleOpen :: FilePath -> OpenMode -> OpenFileFlags -> IO (Either Errno File)
googleOpen path _ _ = do
  logMessage $ "Opening file: " ++ path
  handleApi (googleFile path) (Left eNOENT) $ \mf -> do
    case mf of
      Nothing -> do
        logMessage "Cannot open file"
        return (Left eNOENT)
      Just f ->do
        logMessage $ "Opened FileId" ++ show (fileId f)
        return (Right f)


googleRead :: FilePath -> File -> ByteCount -> FileOffset -> IO (Either Errno ByteString)
googleRead path f size offset = do
  logMessage $ "Reading file: " ++ path
  logMessage $ "Reading fileId: " ++ (show $ fileId f)
  logMessage $ "Reading with ByteCount: " ++ show size
  logMessage $ "Reading with Offset: " ++ show offset
  token <- googleToken
  logMessage token
  result <- makeCurl token f size offset
  -- logMessage $ show result
  return $ B.take (fromIntegral size) . B.pack <$> result

--   handleApi (downloadFile f sink) (Left eNOENT) $ \mf -> do
--     case mf of
--       Nothing -> do
--         logMessage "No file found"
--         return $ Left eNOENT
--       Just b -> do
--         logMessage (show b)
--         return $ Right b
-- --    handleMaybe (Left eNOENT) (return . Right) mf
--   where
--     sink = ($$+- DCC.fold)

googleWrite :: FilePath -> File -> ByteString -> FileOffset -> IO (Either Errno ByteCount)
googleWrite _ f contents _ = do
  token <- googleToken
  logMessage $ "Writing to file"
  result <- updateCurl token f contents
  return $ fromIntegral . length <$> result

splitHeader' :: String -> String -> (String, String)
splitHeader' [] header = (reverse header, "")
splitHeader' ('\r':'\n':'\r':'\n':body) header = (reverse header,body)
splitHeader' (x:xs) header = splitHeader' xs (x:header)

splitHeader :: String -> (String, String)
splitHeader = flip splitHeader' ""

makeCurl :: String -> File -> ByteCount -> FileOffset -> IO (Either Errno String)
makeCurl token f size offset = do
  result <- readProcess "/usr/local/bin/curl" [
    "-L", "--include",
    "-H", "Authorization: Bearer " ++ token,
    "-H", "Range: bytes=" ++ show offset ++ "-" ++ show (offset + fromIntegral size),
    "https://www.googleapis.com/drive/v3/files/" ++ T.unpack (fileId f) ++ "?alt=media"] ""
  let (header, body) = splitHeader result
  logMessage $ show header
  if B.pack "HTTP/1.1 20" `B.isPrefixOf` B.pack header
    then do
      logMessage "File Read"
      return $ Right body
    else do
      logMessage "File Failed"
      return $ Left eNOENT

updateCurl :: String -> File -> ByteString -> IO (Either Errno String)
updateCurl token f content = do
  let contentString = B.unpack content
  result <- readProcess "/usr/local/bin/curl" [
    "-L", "--include",
    "--data-binary", "@-",
    "-H", "Authorization: Bearer " ++ token,
    "-X", "PATCH",
    "https://www.googleapis.com/upload/drive/v3/files/" ++ T.unpack (fileId f) ++ "?uploadType=multipart"] contentString
  let (header, body) = splitHeader result
  logMessage $ show header
  if B.pack "HTTP/1.1 20" `B.isPrefixOf` B.pack header
    then do
      logMessage "File Written"
      return $ Right body
    else do
      logMessage "File Failed"
      logMessage body
      return $ Left eNOENT

googleOpenDirectory :: FilePath -> IO Errno
googleOpenDirectory "/" = return eOK
googleOpenDirectory path = do
  handleApi (googleFile path) eNOENT $ handleMaybe eNOENT $ \f ->
        if isFolder f
        then return eOK
        else return eNOENT

googleReadDirectory :: FilePath -> IO (Either Errno [(FilePath, FileStat)])
googleReadDirectory path = do
  ctx <- getFuseContext
  handleApi (handleMaybe [] googleListDirectory =<< pathToFile path) (Left eNOENT) $ \fs -> do
      return $ Right $ [(".",          defaultDirectory)
                       ,("..",         defaultDirectory)
                       ] ++ map (\f -> (T.unpack . fileTitle . fileData $ f,
                                      googleFileStat ctx f)) fs

readDirectory :: FileId -> Api [File]
readDirectory path = do
  mf <- getFile path
  case mf of
    Just f -> listVisibleContents f
    Nothing -> do
      logMessage "No such directory"
      throwApiError "No Such Directory"

googleGetFileSystemStats :: FilePath -> IO (Either Errno FileSystemStats)
googleGetFileSystemStats = undefined

someFunc :: IO ()
someFunc = do
  token <- googleToken
  print token
  fuseMain googleFuse defaultExceptionHandler

logMessage :: MonadIO m =>String -> m ()
logMessage = liftIO . appendFile logfile . (++ "\n")

googleCreateDevice :: FilePath -> EntryType -> FileMode -> DeviceID -> IO Errno
googleCreateDevice path _ _ _ = do
  logMessage "Create Device"
  let (parent,title) = splitFileName path
  handleApi (do
    mp <- pathToFile $ "/" ++ parent
    case mp of
      Nothing -> return eNOENT
      Just p -> do
        _ <- createFile ((newFile (T.pack title) Nothing) {fileParents = [fileId p]})
        return eOK)
    eNOENT
    return

googleCreateFolder :: FilePath -> FileMode -> IO Errno
googleCreateFolder path _ = do
  logMessage "Create Folder"
  let (parent,title) = splitFileName path
  handleApi (do
    mp <- pathToFile $ "/" ++ parent
    case mp of
      Nothing -> return eNOENT
      Just p -> do
        _ <- createFile ((newFile (T.pack title) Nothing) {
                           fileParents = [fileId p],
                           fileMimeType = T.pack "application/vnd.google-apps.folder"})
        return eOK)
    eNOENT
    return
